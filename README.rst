ics-ans-archappl
================

Ansible playbook to install `EPICS Archiver Appliance <http://slacmshankar.github.io/epicsarchiver_docs/index.html>`_ on CentOS 7.3.

Variables
---------

Default variables are set in `group_vars/archiver_appliance`::

    epicsarchiverap_db_name: archappl
    epicsarchiverap_db_user: archappl
    epicsarchiverap_db_password: archappl

    mariadb_databases:
      - name: "{{ epicsarchiverap_db_name }}"
        init_script: create_archappl_tables.sql

    mariadb_users:
      - name: "{{ epicsarchiverap_db_user }}"
        password: "{{ epicsarchiverap_db_password }}"
        priv: "{{ epicsarchiverap_db_name }}.*:ALL"


This is the minimum to pass to create and initialize the database.

Check the `ics-ans-role-mariadb <https://bitbucket.org/europeanspallationsource/ics-ans-role-mariadb>`_
and `ics-ans-role-epicsarchiverap <https://bitbucket.org/europeanspallationsource/ics-ans-role-epicsarchiverap>`_ roles
for the complete list of variables you can change.


Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A script is provided to easily provision a machine.
It will install ansible locally.
This is for testing purpose and not recommended for production.

Download the script `bootstrap-local.sh` and run it as root::

    $ cd /tmp
    $ curl -O https://bitbucket.org/europeanspallationsource/ics-ans-archappl/raw/master/bootstrap-local.sh
    $ chmod a+x /tmp/bootstrap-local.sh
    $ sudo /tmp/bootstrap-local.sh

This will use the default variables. If you want to make changes, you can use the "--no-run" option
and run manually the ansible-playbook command::

    $ sudo /tmp/bootstrap-local.sh --no-run
    Set variables in /etc/ansible/host_vars/localhost
    $ sudo /usr/local/bin/ansible-playbook /etc/ansible/playbook.yml


To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.4.0.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://bitbucket.org/europeanspallationsource/ics-ans-archappl.git
    $ cd ics-ans-archappl
    # Edit the "hosts" file
    # Change the database password in group_vars/archiver_appliance or create a host_vars/<hostname> file
    # with the variables to change
    $ make playbook


Testing
-------

The playbook is tested using `molecule <https://molecule.readthedocs.io/en/master/>`_ (1.24 <= version < 2.0)::

    $ molecule test


License
-------

BSD 2-clause

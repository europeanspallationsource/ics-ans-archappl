import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('aa_cluster01')


# Prove hosts are connected to each other
def test_archappl_metrics_list(archappl_metrics):
    assert len(archappl_metrics) == 2
    instances = sorted([item['instance'] for item in archappl_metrics])
    assert ['appliance0_ics-ans-archappl-cluster-01', 'appliance0_ics-ans-archappl-cluster-02'] == instances

import time
import pytest
import json


@pytest.fixture(scope='module')
def archappl_index(host):
    """Fixture that returns the archiver appliance index

    Wait at least 60 seconds for the service to be up.
    Depending on the hardware we run the tests on, the startup
    can take quite some time...
    """
    for i in range(60):
        cmd = host.run('curl http://localhost:17665/mgmt/ui/index.html')
        if 'service is not currently available' not in cmd.stdout:
            break
        time.sleep(1)
    return cmd.stdout


@pytest.fixture(scope='module')
def archappl_metrics(host, archappl_index):
    cmd = host.run('curl http://localhost:17665/mgmt/bpl/getInstanceMetrics')
    output = json.loads(cmd.stdout)
    return output

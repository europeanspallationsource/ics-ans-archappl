import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-archappl-default')


def test_archappl_metrics_list(archappl_metrics):
    assert len(archappl_metrics) == 1
    assert 'appliance0' == archappl_metrics[0]['instance']
